%% script for simulation results in CDC 2021
clear all %#ok
close all

%% load parameters
run problem_parameters


%% decide number of samples 

eps = 0.05;
eps_uniform = eps/(T*O);
beta = 1e-3;
beta_uniform = beta/(T*O);

N_scenario = ceil(1.59/eps * (log(2^(L*T*O)/beta) + T*nu - 1));
N_proposed = ceil(1.59/eps * (log(1/beta) + L*K*O*T - 1));

%% Set problem parameters
params.goal = goal;
params.lane = lane;
params.car.d  = car.d;
params.car.v = car.v;
params.x0 = x0;
params.truck.d = truck.d;
params.truck.theta0 = truck.theta0;
params.truck.start = truck.start;
params.truck.v = truck.v;
params.xmin = xmin;
params.xmax = xmax;
params.xmax_bold = xmax_bold;
params.xmin_bold = xmin_bold;
params.vmin = vmin;
params.vmax = vmax;
params.umin = umin;
params.umax = umax;
params.T = T;
params.middle = middle;
params.nx = nx;
params.nu = nu;
params.nw = nw;
params.A = A;
params.B = B;
params.Gamma = Gamma;
params.States_free_init = States_free_init;
params.c1 = c1;
params.c2 = c2;
params.c3 = c3;
params.t1 = t1;
params.t2 = t2;
params.t3 = t3;
params.diag = diag;
params.Ts = Ts;
params.O = O;
params.L = L;
params.K = K;
params.N_scenario = N_scenario;
params.N_proposed = N_proposed;
params.N = max(N_scenario, N_proposed);

%% Generate samples of obstacles
% draw new multiple samples
% w_vec = generate_samples(params, params.N);
% save('Saved_data/w_bimodal.mat', 'w_vec');
load('Saved_data/w_bimodal.mat');



%% Scenario approach and our approach
fprintf(['-------------------------------------------------------------------------\n',...
        'Solve the scenario problem ...\n']);

[u_scenario, cost_scenario, DIAGNOSTIC_scenario] = solve_scenario(params, w_vec);
compTime_scenario = DIAGNOSTIC_scenario.solvertime;

fprintf('Completed.\n');

fprintf(['-------------------------------------------------------------------------\n',...
        'Solve the proposed problem ...\n']);

[u_proposed, cost_proposed, A_union, b_union, t_overapprox, DIAGNOSTIC_proposed] = solve_proposed(params, w_vec);
% compTime_proposed = DIAGNOSTIC_proposed.solvertime;
% cost_proposed = DIAGNOSTIC_proposed.;

for t=1:T
    for clu = 1:K
        H1 = A_union{clu, t};
        h1 = b_union{clu, t};
        v_overapprox{clu, t} = Polyhedron(H1, h1).V;
        p_overapprox{clu, t} = polyshape([v_overapprox{clu, t}(:,1); v_overapprox{clu, t}(1,1)], [v_overapprox{clu, t}(:,2); v_overapprox{clu, t}(1,2)]);
    end
end

fprintf('Completed.\n');

%% Test the policy with a new realization 
w_new = generate_samples(params, 1); 

[car_states_scenario, truck_states]= task_for_new_realization(u_scenario, params, w_new);
[car_states_proposed, ~]= task_for_new_realization(u_proposed, params, w_new);

%% Plots
fontsize = 18;
ind_frames = [1, 6, 9, 11];
N_frames = length(ind_frames);
xlim_vec = repmat([0, 100],length(ind_frames),1);
ylim_vec = repmat([-25,5],length(ind_frames),1);
h = figure();
for i = 1 : N_frames
    ind = ind_frames(i);
    subplot(2,2,i); hold on; grid on
    title(['$$t=$$', num2str(ind-1)], 'interpreter', 'latex')
    plot(car_states_scenario(1,1:ind), car_states_scenario(2,1:ind), '-s', 'Color', 'm', 'LineWidth', 0.5)
    plot(car_states_proposed(1,1:ind), car_states_proposed(2,1:ind), 'b-*','LineWidth',0.5)
    v_car_scenario = generate_vertex(params, 'car', car_states_scenario(:,ind));
    p_car_scenario = polyshape(v_car_scenario.x, v_car_scenario.y);
    v_car_proposed = generate_vertex(params, 'car', car_states_proposed(:,ind));
    p_car_proposed = polyshape(v_car_proposed.x, v_car_proposed.y);
    plot(p_car_scenario,'EdgeColor', 'm', 'FaceColor', 'none', 'LineWidth', 1)
    plot(p_car_proposed,'EdgeColor', 'b', 'FaceColor', 'none', 'LineWidth',1)
    
    for j=1:50
        truck_states = reshape(w_vec(:,j), nw, []);
        
        v_truck = generate_vertex(params, 'truck', truck_states(:,ind));
        p_truck = polyshape(v_truck.x, v_truck.y);
        plot(p_truck,'EdgeColor','r','FaceColor', 'none', 'LineWidth',1);
    end
    for clu = 1:K
        if ind > 1
            plot(p_overapprox{clu,ind-1}, 'EdgeColor', 'k', 'FaceColor', 'none', 'LineWidth', 1)
        end
    end
    
    plot(xlim_vec, lane*ones(2,1), '-k', 'LineWidth', 2)
    plot([xlim_vec(1), xlim_vec(end)], -lane*ones(2,1), '-k', 'LineWidth', 2)
 
    plot(xlim_vec, 0*ones(2,1), '--k', 'LineWidth', 1)
    xlim(xlim_vec(i,:))
    ylim(ylim_vec(i,:))
    
    axis equal
    axis([0 55 -5 5])
    xticks(-5:10:45)
    
%     if i ~= N_frames
        set(gca,'xticklabel',[], 'FontSize', fontsize) 
%     else
%         xticklabels({'0','10','20','30','40', '50', '60'})
%     end
    xlabel('$$x_1$$', 'interpreter', 'latex')
    ylabel('$$x_2$$', 'interpreter', 'latex')
%     if i ~= 1
%         legend('hide')
%     else
%         legend({'Scenario','Our approach'}, 'Location', 'southoutside', 'Orientation', 'horizontal', 'interpreter', 'latex');
%     end
    set(gca,'TickLabelInterpreter', 'latex', 'FontSize', fontsize)
end

set(h, 'Position', [100, 100, 800,250], 'Color', 'w')

% saveas(h,'Figures/lane_change2','epsc')

%% Generate empirical violations
emp_violation_scenario = empirical_violations(params, car_states_scenario)
emp_violation_proposed = empirical_violations(params, car_states_proposed)
