function [States, truck_poses ] = task_for_new_realization( u_star, params, w )

nw = params.nw;

% compute positions and orientations of the Truck 
truck_poses = reshape(w, nw, []);

% compute the applied inputs and positions of the Car
nx = params.nx;
nu = params.nu;
Gamma = params.Gamma;
States_free_init = params.States_free_init;

States = States_free_init + Gamma*u_star;
States =  reshape(States, nx,[]);

end

