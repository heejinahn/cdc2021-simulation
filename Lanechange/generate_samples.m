function [w_vec, i_vec] = generate_samples(params, num_samples)
% multimodality: either decelerate (yield) or accelerate (not allow lane
% changing)

T = params.T;
nw = params.nw;
Ts = params.Ts;
truck.start = params.truck.start;
truck.theta0 = params.truck.theta0;
truck.v = params.truck.v;

w = cell(1,num_samples);
i_vec = zeros(1,num_samples);
for k = 1:num_samples
    w{k}.theta = zeros(T+1,1);
    w{k}.v = zeros(T+1,1); 
    w{k}.v(1) = truck.v;
    if rand < 0.5 % decelerate
        a = (-2 - 0.1*randn);
        for i = 1:T
            w{k}.v(i+1) = max(a * Ts+ w{k}.v(i), 5/3.6);
            w{k}.theta(i+1) = 0.01* (-0.5 + 1 * rand);
        end
        i_vec(k) = 2;
    else % accelerate
        a = (2 + 0.1*randn);
        for i = 1:T+1
            w{k}.v(i+1) = min(a * Ts + w{k}.v(i), 40/3.6);
            w{k}.theta(i+1) = 0.01* (-0.5 + 1 * rand);
        end
        i_vec(k) = 1;
    end
end

% transform w to positions and yaw
w_vec = zeros(nw*(T+1),num_samples);

for k = 1:num_samples
    % compute truck poses, for the given w{k}
    pose_init = [truck.start; truck.theta0];
    [tout, pose_out] = ode45( @(tout, pose_out) truck_dyn(tout, pose_out, w{k}.v, w{k}.theta/Ts, Ts, T) , [0:Ts:T*Ts], pose_init);
    truck_poses = pose_out';

    w_vec(1:nw:end,k) = truck_poses(1,:)';
    w_vec(2:nw:end,k) = truck_poses(2,:)';
    w_vec(3:nw:end,k) = truck_poses(3,:)';  %nw*(T+1) dimensional uncertainty
end


end

