function [w_vec, i_vec] = generate_samples(params, num_samples)

T = params.T;
nw = params.nw;
Ts = params.Ts;
truck.start = params.truck.start;
truck.theta0 = params.truck.theta0;

w = cell(1,num_samples);
i_vec = zeros(1,num_samples);
for k = 1:num_samples
    w{k}.theta = zeros(T+1,1);
    c = 1;
    if rand < 0.5
        for i = 1:T
            w{k}.theta(i) = pi/2/(T/c+1) - 0.03  +  min(0.06, pi/2-sum(w{k}.theta)- pi/2/(T/c+1) + 0.03)*rand;
        end
        w{k}.theta(T+1) = pi/2 - sum(w{k}.theta);
        i_vec(k) = 2;
    else
        for i = 1:T+1
            w{k}.theta(i) = 0.01* (-0.5 + 1 * rand);
        end
        i_vec(k) = 1;
    end
    w{k}.v = -20 * c/3.6*ones(T+1,1);    % the truck has constant linear velocity at -20 Km/h

end

% transform w to positions and yaw
w_vec = zeros(nw*(T+1),num_samples);

for k = 1:num_samples
    % compute truck poses, for the given w{k}
    pose_init = [truck.start; truck.theta0];
    [tout, pose_out] = ode45( @(tout, pose_out) truck_dyn(tout, pose_out, w{k}.v, w{k}.theta/Ts, Ts, T) , [0:Ts:T*Ts], pose_init);
    truck_poses = pose_out';

    w_vec(1:nw:end,k) = truck_poses(1,:)';
    w_vec(2:nw:end,k) = truck_poses(2,:)';
    w_vec(3:nw:end,k) = truck_poses(3,:)';  %nw*(T+1) dimensional uncertainty
end


end

