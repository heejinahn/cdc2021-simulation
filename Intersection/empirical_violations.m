function out = empirical_violations( params, car_x )


nw = params.nw;
T = params.T;
diag = params.diag;
truck.d = params.truck.d;
car.d = params.car.d;


%%
N_samples = 1e5;
N_violation = 0;

% [w_emp, i_emp] = generate_samples(params, N_samples);
% save('Saved_data/w_empirical.mat', 'w_emp', 'i_emp');
load('Saved_data/w_empirical.mat');

for i=1:N_samples
    found_collision = false;

    for t = 1:T+1
        truck_y1 = w_emp(nw*(t-1)+1,i);
        truck_y2 = w_emp(nw*(t-1)+2,i);
        truck_theta = w_emp(nw*(t-1)+3,i);
        
        vertex = generate_vertex(params, 'car', car_x(:,t));
        H1 = [eye(2,2); -eye(2,2)] * [cos(truck_theta), sin(truck_theta); -sin(truck_theta), cos(truck_theta)];
        h1 = [eye(2,2); -eye(2,2)] * [cos(truck_theta), sin(truck_theta); -sin(truck_theta), cos(truck_theta)] * [truck_y1; truck_y2] + 1/2 * [truck.d(1); truck.d(2); truck.d(1); truck.d(2)];
        
        for k=1:length(vertex.x)-1
            vertex_k = [vertex.x(k); vertex.y(k)];
            if all(H1 * vertex_k <= h1)
                t
                found_collision = true;
            end
        end
        
        if found_collision
            N_violation = N_violation + 1;
            break
        end

    end
end


out = N_violation / N_samples;
end

