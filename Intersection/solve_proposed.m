function [ u_star, cost, A_union, b_union, t_overapprox, DIAGNOSTIC ] = solve_proposed(params, w_vec)
%%  Load needed parameters
car.d = params.car.d ;
truck.d = params.truck.d ;
truck.theta0 = params.truck.theta0;
truck.start = params.truck.start ;
xmax_bold = params.xmax_bold;
xmin_bold = params.xmin_bold;
T = params.T;
O = params.O;
L = params.L;
nx = params.nx;
nu = params.nu;
nw = params.nw;
Gamma = params.Gamma ;
States_free_init = params.States_free_init;
c1 = params.c1;
c2 = params.c2;
c3 = params.c3;
t1 = params.t1;
t2 = params.t2;
t3 = params.t3;
diag = params.diag;
K = params.K;
N_proposed = params.N_proposed;

%% Compute the approximation of the union of obstacle sets
% Find vertexes of sampled obstacle sets
draw_sample = 0;
vertex = cell(T,1);
for t=1:T
    w_vec_k = w_vec(nw*t+(1:3), :);
    for k=1:N_proposed
        truck_y1 = w_vec_k(1,k);
        truck_y2 = w_vec_k(2,k);
        truck_theta = w_vec_k(3,k);

 
    if draw_sample == 1
        if rand(1) < 0.1
            H1 = [eye(2,2); -eye(2,2)] * [cos(truck_theta), sin(truck_theta); -sin(truck_theta), cos(truck_theta)];
            h1 = [eye(2,2); -eye(2,2)] * [cos(truck_theta), sin(truck_theta); -sin(truck_theta), cos(truck_theta)] * [truck_y1; truck_y2] + 1/2 * [truck.d(1); truck.d(2); truck.d(1); truck.d(2)] + repmat(diag, 4,1);
            P1 = Polyhedron(H1, h1);
            hold on
            P1.plot('alpha', 0.2, 'color', 'black')
        end
    end
    
    vertex{t}(1:2,k) = [truck_y1; truck_y2] + 1/2 * truck.d(1) * [cos(truck_theta); sin(truck_theta)] + 1/2 * truck.d(2) * [-sin(truck_theta); cos(truck_theta)];
    vertex{t}(3:4,k) = [truck_y1; truck_y2] + 1/2 * truck.d(1) * [cos(truck_theta); sin(truck_theta)] + 1/2 * truck.d(2) * [sin(truck_theta); -cos(truck_theta)];
    vertex{t}(5:6,k) = [truck_y1; truck_y2] - 1/2 * truck.d(1) * [cos(truck_theta); sin(truck_theta)] + 1/2 * truck.d(2) * [-sin(truck_theta); cos(truck_theta)];
    vertex{t}(7:8,k) = [truck_y1; truck_y2] - 1/2 * truck.d(1) * [cos(truck_theta); sin(truck_theta)] + 1/2 * truck.d(2) * [sin(truck_theta); -cos(truck_theta)];
    end
end

% Cluster the samples
t_overapprox_start = tic;
num_cluster = K;

w_vec_N = w_vec(nw * T + (1:3),:); % samples at last time
[idx_cluster, Centers] = kmeans(w_vec_N', num_cluster); % clustering performed only once at the last prediction time
if abs(Centers(1,3)) < abs(Centers(2,3)) % cluster 1 corresponds to go straight
else
    idx_cluster = 3 - idx_cluster;
end
    
A_union = cell(num_cluster,T);
b_union = cell(num_cluster,T);
for k=1:num_cluster
    for t= 1 : T
        w_vec_cluster_k = w_vec(nw * t + (1:3),idx_cluster == k);
        vertex_k = vertex{t}(:, idx_cluster == k);
        [A_union_k, b_union_k] = compute_approximate_union(w_vec_cluster_k, vertex_k);
        A_union{k,t} = A_union_k;
        b_union{k,t} = b_union_k;
    end
end
t_overapprox = toc(t_overapprox_start);
fprintf(' The overapproximating obstacle set computation: %1.4f sec.\n', t_overapprox);



%% Motion planning problem
%%% Optimization variables

u = sdpvar(nu*T,1,'full'); % input sequence 
delta = binvar(L*K,T); % binary variables for obstacle 

x_future = States_free_init +  Gamma*u;
big_M = 200; % conservative upper-bound

%%% Constraints
constraints = [];
constraints_obs = [];

x1 = x_future(nx+1:nx:end,:);
x2 = x_future(nx+2:nx:end,:);
x3 = x_future(nx+3:nx:end,:);
x4 = x_future(nx+4:nx:end,:);


constraints = [constraints , x1  <=  xmax_bold(nx+1:nx:end), ...
    - x1 <= - xmin_bold(nx+1:nx:end), ...
    x2  <=  xmax_bold(nx+2:nx:end) , ...
    - x2 <= - xmin_bold(nx+2:nx:end)];



%%%%%%% 3rd and 4th states have COUPLED CONSTRAINTS: ...
%       x4 - c1*x3 <= - c3
%       x4 - c1*x3 >= - c2
%       x4 + c1*x3 <= c2
%       x4 + c1*x3 >= c3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

constraints = [constraints , x4 - c1*x3 <=  - repmat(c3, T, 1) , ...
    - x4 + c1*x3  <=   repmat(c2, T, 1),...
    x4 + c1*x3  <=   repmat(c2, T, 1)
    - x4 - c1*x3  <=  - repmat(c3, T, 1) ];


%%% Inputs have COUPLED CONSTRAINTS:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       u2 - t1*u1 <= - t3
%       u2 - t1*u1 >= - t2
%       u2 + t1*u1 <= t2
%       u2 + t1*u1 >= t3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
u1 = u(1:nu:end);
u2 = u(2:nu:end);

constraints = [constraints , u2 - t1*u1  <=  - repmat(t3, T, 1) , ...
    - u2 + t1*u1  <=   repmat(t2, T, 1),...
    u2 + t1*u1  <=   repmat(t2, T, 1)
    - u2 - t1*u1  <=  - repmat(t3, T, 1) ];

for t = 1:T
    for clu = 1:K
        A_obs = A_union{clu,t};
        b_obs = b_union{clu,t};
        
        if clu == 1 % truck going straight
        constraints_obs = [constraints_obs , ...
            A_obs * [x1(t); x2(t)] + big_M*(1-delta(4*(clu-1)+(1:4),t)) >= b_obs + repmat(car.d(2)/2 * 1.1, size(b_obs)), ...
            sum(delta(4*(clu-1)+(1:4),t)) >= 1];
        elseif clu == 2 % truck turning
        constraints_obs = [constraints_obs , ...
            A_obs * [x1(t); x2(t)] + big_M*(1-delta(4*(clu-1)+(1:4),t)) >= b_obs + repmat(diag, size(b_obs)), ...
            sum(delta(4*(clu-1)+(1:4),t)) >= 1];
        end    
        
    end
end

Constraints = [constraints, constraints_obs];


%%% objective

objective = - ( x1(end) );

%%% Solve the problem
ops = sdpsettings;
ops.solver = 'cplex';
ops.verbose = 3;
ops.debug = 1;

DIAGNOSTIC = optimize(Constraints, objective, ops)



%% Save results
u_star = value(u);
cost = value(objective);

end

