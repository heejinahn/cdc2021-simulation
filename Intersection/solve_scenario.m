function [ u_star, cost, DIAGNOSTIC ] = solve_scenario(params, w)
%%  Load needed parameters
car.d = params.car.d ;
truck.d = params.truck.d ;
truck.theta0 = params.truck.theta0;
truck.start = params.truck.start ;
xmax_bold = params.xmax_bold;
xmin_bold = params.xmin_bold;
T = params.T;
O = params.O;
L = params.L;
nx = params.nx;
nu = params.nu;
nw = params.nw;
Gamma = params.Gamma ;
States_free_init = params.States_free_init;
c1 = params.c1;
c2 = params.c2;
c3 = params.c3;
t1 = params.t1;
t2 = params.t2;
t3 = params.t3;
diag = params.diag;
N_scenario = params.N_scenario;

%% Optimization variables

u = sdpvar(nu*T,1,'full');
States_free = States_free_init +  Gamma*u;

% binary variables for obstacle
delta = binvar(L,T);
big_M = 200; % conservative upper-bound

% sampling
w_vec = w(:,1:N_scenario);

%% Constraints
constraints = [];
constraints_obs = [];

x_bold = sdpvar(nx*T,1,'full');
x_future = States_free_init + Gamma * u;

% state box constraints
constraints = [constraints,...
    x_bold == x_future(nx+1:end,:)];

x1_bold = x_bold(1:nx:end,:);
x2_bold = x_bold(2:nx:end,:);
x3_bold = x_bold(3:nx:end,:);
x4_bold = x_bold(4:nx:end,:);

constraints = [constraints , x1_bold  <=  xmax_bold(nx+1:nx:end) , ...
    - x1_bold <= - xmin_bold(nx+1:nx:end), ...
    x2_bold  <=  xmax_bold(nx+2:nx:end), ...
    - x2_bold <= - xmin_bold(nx+2:nx:end)];

%%%%%%% 3rd and 4th states have COUPLED CONSTRAINTS: ...
%       x4 - c1*x3 <= - c3
%       x4 - c1*x3 >= - c2
%       x4 + c1*x3 <= c2
%       x4 + c1*x3 >= c3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

constraints = [constraints , x4_bold - c1*x3_bold  <=  - repmat(c3, T, 1) , ...
    - x4_bold + c1*x3_bold  <=   repmat(c2, T, 1),...
    x4_bold + c1*x3_bold  <=   repmat(c2, T, 1)
    - x4_bold - c1*x3_bold  <=  - repmat(c3, T, 1) ];

%% Inputs have COUPLED CONSTRAINTS:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       u2 - t1*u1 <= - t3
%       u2 - t1*u1 >= - t2
%       u2 + t1*u1 <= t2
%       u2 + t1*u1 >= t3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
u1_bold = u(1:nu:end,1);
u2_bold = u(2:nu:end,1);

constraints = [constraints , u2_bold - t1*u1_bold  <=  - repmat(t3, T, 1) , ...
    - u2_bold + t1*u1_bold  <=   repmat(t2, T, 1),...
    u2_bold + t1*u1_bold  <=   repmat(t2, T, 1)
    - u2_bold - t1*u1_bold  <=  - repmat(t3, T, 1) ];

for k = 1:N_scenario
    
    % truck positions and orientations, given the sample w, from
    % the second time step
    
    truck_y1 = w_vec(nw+1:nw:end,k);
    truck_y2 = w_vec(nw+2:nw:end,k);
    truck_theta = w_vec(nw+3:nw:end,k);
    
%     constraints_obs = [constraints_obs , ...
%         (  x2_bold - truck_y2 ).*cos(truck_theta) + diag <= (sin(truck_theta).*(x1_bold - truck_y1  ) - truck.d(2)/2 ) + big_M*(1-delta(1,:)') , ...
%         (- x2_bold + truck_y2 ).*cos(truck_theta) + diag <= ( - sin(truck_theta).*(x1_bold - truck_y1 ) - truck.d(2)/2) + big_M*(1-delta(2,:)') , ...
%         ( x2_bold - truck_y2 ).*sin(truck_theta) + diag <= ( - cos(truck_theta).*(x1_bold - truck_y1 ) - truck.d(1)/2) + big_M*(1-delta(3,:)') , ...
%         (- x2_bold + truck_y2 ).*sin(truck_theta) + diag <= ( cos(truck_theta).*(x1_bold - truck_y1 ) - truck.d(1)/2) + big_M*(1-delta(4,:)') , ...
%         sum(delta,1) == 1];
    
    constraints_obs = [constraints_obs , ...
        (  x2_bold - truck_y2 ).*cos(truck_theta) <= (sin(truck_theta).*(x1_bold - truck_y1  ) - truck.d(2)/2 ) + big_M*(1-delta(1,:)') , ...
        (- x2_bold + truck_y2 ).*cos(truck_theta) <= ( - sin(truck_theta).*(x1_bold - truck_y1 ) - truck.d(2)/2) + big_M*(1-delta(2,:)') , ...
        ( x2_bold - truck_y2 ).*sin(truck_theta) <= ( - cos(truck_theta).*(x1_bold - truck_y1 ) - truck.d(1)/2) + big_M*(1-delta(3,:)') , ...
        (- x2_bold + truck_y2 ).*sin(truck_theta) <= ( cos(truck_theta).*(x1_bold - truck_y1 ) - truck.d(1)/2) + big_M*(1-delta(4,:)') , ...
        sum(delta,1) >= 1];

end

Constraints = [constraints, constraints_obs];


%% objective

objective = - x1_bold(end);


%% Solve the problem
ops = sdpsettings;
ops.solver = 'cplex';
ops.verbose = 3;
ops.debug = 1;
DIAGNOSTIC = optimize(Constraints, objective, ops)


%% Save results
u_star = value(u);
cost = value(objective);

end

